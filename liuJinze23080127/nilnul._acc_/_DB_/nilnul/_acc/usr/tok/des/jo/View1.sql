﻿CREATE VIEW [nilnul._acc.usr.tok].[Jo]
	AS
select 
	u.*
	,
	t.id tokenId,t.token.t._memo token_memo
	,t._time tokenTime
	from [nilnul._acc].Usr as u --alias
		left join
		[nilnul._acc.usr].Token  t
		on  u.id =t.usr
