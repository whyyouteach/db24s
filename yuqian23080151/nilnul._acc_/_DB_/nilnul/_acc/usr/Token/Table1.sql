﻿CREATE TABLE [nilnul._acc.usr].[Token]
(
	[Id] bigINT NOT NULL PRIMARY KEY identity
	,
	[usr] bigint  references [nilnul].[Usr](id)
	,
	[token] UNiqueIDENTIFIER default newid()
	,
	_time datetime default getUtcdate()
	,
	_memo nvarchar(max)
)
