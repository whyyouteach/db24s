﻿create database my2
	on primary(
		name=my2_data
		,
		fileName=N'c:\dbSrv\my2_data.mdf'
	
	)
	log on(
		name=my2_log
		, 
		fileName=N'c:\dbSrv\my2_log.ldf'
	)