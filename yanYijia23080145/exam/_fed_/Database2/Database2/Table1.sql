﻿CREATE TABLE [dbo].[Tabel]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[姓名] nvarchar(400)
	, 
    [学号] NVARCHAR(400) NULL 
	,
	[班级] datetime default getUtcDate()
	,
	[身份证号] nvarchar(max)
)
