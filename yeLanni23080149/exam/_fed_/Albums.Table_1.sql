﻿CREATE TABLE Albums (  
    AlbumId INT PRIMARY KEY IDENTITY(1,1),  
    Title NVARCHAR(100) NOT NULL,  
    Year INT,  
    ArtistId INT,  
    FOREIGN KEY (ArtistId) REFERENCES Artists(ArtistId)  
);