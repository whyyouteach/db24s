﻿CREATE TABLE [dbo].[Table1]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [班级] NVARCHAR(MAX) NOT NULL, 
    [学号] NVARCHAR(MAX) NOT NULL, 
    [姓名] NVARCHAR(MAX) NOT NULL, 
    [邮箱] VARCHAR(MAX) NOT NULL, 
    [电话号码] NVARCHAR(MAX) NULL
)
