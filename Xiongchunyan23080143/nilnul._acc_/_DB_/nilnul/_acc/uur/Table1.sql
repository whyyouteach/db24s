﻿CREATE TABLE [nilnul].[Usr]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
    [name] NVARCHAR(400) 
	,
    [pass_tip] NVARCHAR(400) NULL 
	,
	_time datetime default getUtcDate()
	,
	_memo nvarchar(max)
)
