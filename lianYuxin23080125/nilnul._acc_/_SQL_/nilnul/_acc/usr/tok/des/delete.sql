﻿declare @usr bigint;
set @usr =3;

set xact_abort on;
begin transaction
begin try
	delete 
		[nilnul._acc.usr].Token
		output deleted.*
		where usr=@usr;

	--throw 50000, N'err',0;

	delete [nilnul._acc].Usr
		output deleted.*
		where id =@usr;
	commit; -- tran
end try
begin catch
	rollback;
	throw;
end catch
