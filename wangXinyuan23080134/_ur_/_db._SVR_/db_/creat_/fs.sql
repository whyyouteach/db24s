﻿create database my1
    on primary (
        name=my1_date
        ,
        fileName=N'c:\dbsrv\my1_date.mdf'
    )
    log on(
        name=my1_log
        ,
        fileName=N'c:\dbsrv\my1_log.ldf'
    )
;