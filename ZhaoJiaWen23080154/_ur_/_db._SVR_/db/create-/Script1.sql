﻿create database my1
    on primary(
        name=my1_data
        ,
        fileName=N'c:\bdsrv\my1_data.mdf'
    )
    log on(
        name=my1_log
        ,
        fileName=N'c:\bdsrv\my1_log.1df'
    )
    