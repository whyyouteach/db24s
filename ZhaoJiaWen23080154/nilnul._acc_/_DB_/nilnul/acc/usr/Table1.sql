﻿CREATE TABLE [nilnul].[Usr]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[name] nvarchar(400)
	,
	[pass_tip] NVARCHAR(400) NULL
	,
	_time datetime default getUtcDate()
	,
	_meno nvarchar(max)
)
