﻿declare @usr bigint;
set @usr =10;

delete
    [nilnul._acc.usr].Token
    output deleted.*
    where usr=@usr;
throw 50000,N'err',0;

delete [nilnul._acc].Usr
    output deleted.*
    where id =@usr;