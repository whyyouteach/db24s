﻿declare @usr bigint;
set @usr=10;
begin transaction
begin try

	delete
		[nilnul._acc.usr].Token
		output deleted.*
		where usr=@usr;


	delete [nilnul].Usr
		output deleted.*
		where id = @usr;
	commit tran
end try
begin catch
	rollback;
	throw;
end catch