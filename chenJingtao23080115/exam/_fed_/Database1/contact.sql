﻿CREATE TABLE [dbo].[contact]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [姓名] NCHAR(10) NULL, 
    [qq] NCHAR(10) NULL, 
    [微信] NCHAR(10) NULL, 
    [电话] NCHAR(10) NULL
)
