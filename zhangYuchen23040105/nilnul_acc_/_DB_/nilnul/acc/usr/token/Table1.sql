﻿CREATE TABLE [nilnul._acc_usr].[Token]
(
	[Id] bigINT NOT NULL PRIMARY KEY identity
	,
	[usr] bigint references [nilnul].[Usr](Id)
	,
	[token] UniqueIDENTIFIER DEFAULT newid()
	,
	_time datetime default getUtcDate()
	,
	_memo nvarchar(max)
)
