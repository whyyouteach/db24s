﻿set xact_abort on;
begin transaction
begin try
	declare @ids table(id bigint);
	insert [nilnul._acc].Usr
		(name,pass_tip)
		output inserted.id into @ids
		output inserted.*
		values(
			N'zs'
			,
			N'123'
		)
	;
	--throw 50000, N'err',0;
	insert 
		[nilnul._acc.usr].Token
		(usr)
		output inserted.*
		values(
			(select * from @ids)
		)

	commit; -- tran
end try
begin catch
	rollback;
	throw;
end catch
