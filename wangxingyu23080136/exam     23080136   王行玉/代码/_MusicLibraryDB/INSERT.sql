﻿-- 添加一些数据到艺术家表  
INSERT INTO Artists (Name, Description, ImageUrl)  
VALUES   
    ('Justin Bieber', 'Canadian singer and songwriter', 'https://example.com/bieber.jpg'),  
    ('Taylor Swift', 'American singer-songwriter', 'https://example.com/swift.jpg'),  
    ('Justin Timberlake', 'American singer, songwriter, actor, and record producer', 'https://example.com/timberlake.jpg');  
  
-- 添加一些数据到专辑表 
INSERT INTO Albums (Title, ReleaseDate, ArtistID)  
VALUES   
    ('Purpose', '2015-11-13', (SELECT ArtistID FROM Artists WHERE Name = 'Justin Bieber')),  
    ('Lover', '2019-08-23', (SELECT ArtistID FROM Artists WHERE Name = 'Taylor Swift')),  
    ('Man of the Woods', '2018-02-02', (SELECT ArtistID FROM Artists WHERE Name = 'Justin Timberlake'));  
  
-- 添加一些数据到曲目表  
INSERT INTO Tracks (Title, Duration, AlbumID, Number)  
VALUES   
    ('What Do You Mean?', '03:34', (SELECT AlbumID FROM Albums WHERE Title = 'Purpose' AND ArtistID = (SELECT ArtistID FROM Artists WHERE Name = 'Justin Bieber')), 1),  
    ('Lover', '03:51', (SELECT AlbumID FROM Albums WHERE Title = 'Lover' AND ArtistID = (SELECT ArtistID FROM Artists WHERE Name = 'Taylor Swift')), 1),  
    ('Supplies', '03:29', (SELECT AlbumID FROM Albums WHERE Title = 'Man of the Woods' AND ArtistID = (SELECT ArtistID FROM Artists WHERE Name = 'Justin Timberlake')), 1);  
  