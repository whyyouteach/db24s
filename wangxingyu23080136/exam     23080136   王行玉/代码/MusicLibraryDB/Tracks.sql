﻿CREATE TABLE Tracks (  
    TrackID INT PRIMARY KEY IDENTITY(1,1),  
    Title NVARCHAR(255) NOT NULL,  
    Duration TIME,  
    AlbumID INT,  
    Number INT,   
    FOREIGN KEY (AlbumID) REFERENCES Albums(AlbumID)  
);