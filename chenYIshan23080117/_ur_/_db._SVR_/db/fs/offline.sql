﻿ALTER DATABASE [my2]
	SET offline  -- let the files off the svr which provisions db;  keep db, but unhold files such that they can be maintained as ordinary files
	WITH ROLLBACK IMMEDIATE 
;
