﻿CREATE PROCEDURE InsertNewUsers
    @Username NVARCHAR(50),
    @PasswordHash NVARCHAR(256),
    @Email NVARCHAR(100),
    @FirstName NVARCHAR(50),
    @LastName NVARCHAR(50)
AS
BEGIN
    SET NOCOUNT ON;

    INSERT INTO Users (Username, PasswordHash, Email, FirstName, LastName)
    VALUES (@Username, @PasswordHash, @Email, @FirstName, @LastName);
END
GO
