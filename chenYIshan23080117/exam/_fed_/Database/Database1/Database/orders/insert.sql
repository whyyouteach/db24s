﻿insert [Orders]
	(
		UserId
		,
		OrderId
		,
		Quantity
		,
		OrderDate
	)
	output inserted.*
	values(
		N'1'
		,
		N'123456'
		,
		N'100'
		,
		N'2024'
		)
;

 go 