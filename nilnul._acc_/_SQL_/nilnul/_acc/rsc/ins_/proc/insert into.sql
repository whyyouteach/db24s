﻿declare @ret int;

declare @t table (
	[id] bigINT NOT NULL 
	,
	[urn] nvarchar(4000) -- caption other resources as an abstract term here;
	,
	[_time] datetime default getUtcDate()
	,
	_memo nvarchar(max) --备注; in the for
);

insert into @t
	exec @ret = [nilnul._acc.rsc].Ins 
			N'mobile phone screen lock1234'
;

select @ret;

select * from @t;
