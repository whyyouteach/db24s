﻿declare @urn nvarchar(4000);

set @urn = N'http://nilnul.org';


if exists(
		select 
			*
				from [nilnul._acc].Rsc
		where urn = @urn
	) 
	return;
else 
	begin

		insert [nilnul._acc].Rsc
			(
				urn
			)
			output inserted.*
			values(
				@urn

			)
		;
	end
