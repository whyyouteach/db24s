﻿CREATE TABLE [nilnul].[Acc]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[rsc] bigint  references [nilnul._acc].Rsc(id) --nullable to allow Acc without Rsc
	,
	[name] nvarchar(400) --usr name
	,
	[pass_tip] nvarchar(400) -- pass tip/cue
	,
	_time datetime default getUtcDate()
	,
	_memo nvarchar(max)
	--, 
 --   CONSTRAINT [CK_Acc_name9pass] CHECK (name != pass_tip )
)
GO

/**
 ensure the last(max id) name and pass_tip are not both repeated in any newly-inserted rows;
*/
CREATE TRIGGER [nilnul].[Trigger_Acc_norepeat_last_name]
    ON [nilnul].[Acc]
    -- /*FOR*/ after 
	instead of
	/*DELETE,*/ INSERT /*, UPDATE*/
    AS
    BEGIN
		if exists (  
			select *
				from 
					inserted as x
						join 
					( select *
						from
								[nilnul].[Acc]
					
						where id = (
							select max(id) 
								from [nilnul].[Acc]
						)
					) as y		--alias
						on x.name=y.name and x.pass_tip=y.pass_tip
		)begin
			;
			throw 50000,N'You cannot insert a row which has the same name and pass_tip as the latest one; ',0;
		end
		else 
		begin
			insert [nilnul].Acc
				(rsc, name,pass_tip)
				select rsc, name,pass_tip
					from inserted
		end

        SET NoCount ON
    END
go
