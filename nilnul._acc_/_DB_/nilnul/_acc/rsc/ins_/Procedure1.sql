﻿CREATE PROCEDURE [nilnul._acc.rsc].[Ins]
	@urn nvarchar(4000)
AS
if exists(
		select 
			*
				from [nilnul._acc].Rsc
		where uri = @urn
	) 
	return 1;
else 
	begin
		insert [nilnul._acc].Rsc
			(
				uri
			)
			output inserted.*
			values(
				@urn
			)
		;
		select 777;
		print N'inserted success;';
	end
RETURN 0;
