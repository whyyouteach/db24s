﻿CREATE TABLE [dbo].[my]
(
	[id] bigINT NOT NULL PRIMARY KEY identity
	,
	[software] nvarchar(400)
	, 
    [pass_tip] NVARCHAR(400) NULL 
	,
	_time datetime default getUtcDate()
)