﻿CREATE TABLE [dbo].[Borrowings]
(
	[borrowing-id] INT NOT NULL PRIMARY KEY, 
    [book-id] INT NULL, 
    [borrower-name] VARCHAR(100) NULL, 
    [borrow-date] DATE NULL, 
    [return-date] DATE NULL
)
