﻿CREATE TABLE [nilnul._acc.usr].[Token]
(
	[Id] bigINT NOT NULL PRIMARY KEY identity
	,
	[usr]bigint  references     [nilnul].[Usr](id)
	,
	[token] UniqueIDENTIFIER default newid()
	,
	_time datetime default getUtcDate()
	,
	_memo nvarchar(max)
)
