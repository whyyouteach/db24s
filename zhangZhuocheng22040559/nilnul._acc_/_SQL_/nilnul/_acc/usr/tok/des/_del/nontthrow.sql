﻿declare @usr bigint;
set @usr =1;

delete 
	[nilnul._acc.usr].Token
	output deleted.*
	where usr=@usr;

delete [nilnul._acc].Usr
	output deleted.*
	where id =@usr;